<%@page import="java.util.List" %>
<%@page import="br.com.senac.agenda.model.Contato" %>
<jsp:include page="../Header.jsp"/>

<%
    Contato contato = (Contato) request.getAttribute("contato");
    String mensagens = (String) request.getAttribute("mensagens");
    String erro = (String) request.getAttribute("erro");

    
%>

<% if (mensagens != null) { %>
<div class=" alert alert-success">
    
    <%= mensagens %>
    
    
</div>
<% } %>


<% if ( erro != null) {%>
<div>
    <%= erro%>
    
</div>
    <%}%>
    
<form action="./CadastroContatoServlet" method="post">
    <div class="form-group">
        <label for="textCodigo" style="margin-right: 10px">C�digo:</label>
        <p><input name="codigo" type="text" id="codigo" class="form-control col-2" readonly value=' <%=contato != null ? contato.getCodigo() : "" %>' /></p>
    </div>

    <hr/>

    <div class="form-row">
        <div class="form-group col-md-12">
            <label for="textNome">Nome Completo:</label>
            <input name= "nome" type="text" class="form-control" id="nome" value='<%= contato != null ? contato.getNome(): "" %>' >
        </div>  
    </div>

    <div class="form-row">
        <div class="form-group col-2">
            <label for="textTelefone">Telefone:</label>
            <input name="telefone" type="text" class="form-control" id="telefone" value='<%= contato != null ? contato.getTelefone(): ""%>' >
        </div>

        <div class="form-group col-md-2">
            <label for="textCelular">Celular:</label>
            <input name="celular" type="text" class="form-control" id="celular" value='<%= contato != null ? contato.getCelular(): ""%>' >
        </div>
    </div>  

    <div class="form-row">
        <div class="form-group col-6">
            <label for="textEndere�o">Endere�o:</label>
            <input name="endereco"   type="text" class="form-control" id="endere�o" value='<%= contato != null ? contato.getEndereco():""%>' >
        </div>

        <div class="form-group col-4">
            <label for="textCep">CEP:</label>
            <input name="cep" type="text" class="form-control" id="cep" value='<%= contato != null ? contato.getCep(): "" %>' >
        </div>

        <div class="form-group col-2">
            <label for="textNumero">Numero:</label>
            <input name="numero" type="text" class="form-control col-1" id="numero" <%= contato != null ? contato.getNumero(): "" %> >
        </div>
    </div>
    <div class="form-row">

        <div class="form-group col-4">
            <label for="textBairro">Bairro:</label>
            <input name="bairro"  type="text" class="form-control col-2" id="bairro" value='<%= contato != null ? contato.getBairro(): ""%>'>      
        </div>

        <div class="form-group col-4">
            <label for="textCidade">Cidade:</label>
            <input name="cidade"  type="text" class="form-control col-1" id="cidade" value='<%= contato != null ? contato.getCidade(): ""%>'>      
        </div>

        <div class="form-group col-4">
            <label for="textEstado">Estado:</label>
            <select type="text" id="estado" name="estado" class="form-control" value='<%= contato != null ? contato.getEstado(): ""%>'>
                <option selected>UF</option>
                <option>ES</option>
                <option>SP</option>
                <option>RJ</option>
                <option>MG</option>
                <option>BH</option>
            </select>
        </div> 
    </div>

    <div class="form-row">
        <div class="form-group col-md-12">
            <label for="textEmail">Email:</label>
            <input name="email" type="text" class="form-control" id="email" value='<%= contato != null ? contato.getEmail(): "" %>'>
        </div>  
    </div>
    <hr/>

    <div>
        <button type="submit"  class="btn btn-success">Salvar</button>
        <button type="reset"  class="btn btn-danger">Cancelar</button>
    </div>
</form>

<jsp:include page="../Footer.jsp"/>
