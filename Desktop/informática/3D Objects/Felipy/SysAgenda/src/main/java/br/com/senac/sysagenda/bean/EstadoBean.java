package br.com.senac.sysagenda.bean;

import br.com.senac.sysagenda.dao.EstadoDAO;
import br.com.senac.sysagenda.model.Estado;
import java.io.Serializable;
import javax.inject.Named;
import javax.faces.view.ViewScoped;


@Named(value = "estadoBean")
@ViewScoped
public class EstadoBean implements Serializable{

 
    private Estado estado;
    private final EstadoDAO dao;
    
    public EstadoBean() {
        this.estado = new Estado();
        this.dao = new EstadoDAO();
    }


    public void salvar(){
        
        if (this.estado.getId() == 0 ){
               dao.save(estado);
        }else{
            dao.update(estado);
            
        }
    }
    
    public void novo(){
        this.estado = new Estado();
    }
    

   public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    
}
