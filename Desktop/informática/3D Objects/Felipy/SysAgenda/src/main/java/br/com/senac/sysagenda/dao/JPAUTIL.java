package br.com.senac.sysagenda.dao;

import br.com.senac.sysagenda.model.Estado;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;


public class JPAUTIL {
    
    private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("SysAgendaPU");
    
    public static EntityManager getEntityManager(){
        try{
            return emf.createEntityManager();
            
        }catch(Exception ex){
            ex.printStackTrace();
            throw new RuntimeException("Erro ao acessar banco de dados.");
        }
    
        
    }   
    
    public static void main(String[] args){
        
        Estado estado = new Estado();
        estado.setNome("Espirito Santo");
        estado.setSigla("ES");
        
        EstadoDAO dao = new EstadoDAO();
        dao.save(estado);
        
        Estado estado1 = new Estado();
        estado1.setNome("Rio de Janeiro");
        estado1.setSigla("RJ");
        
        dao.save(estado1);
        
        
        estado1.setNome("São Paulo");
        estado1.setSigla("SP");
        
        dao.update(estado1);
        dao.delete(estado);
        
        Estado sp = dao.find(2);
        
        System.out.println(sp.getNome());
        
        List<Estado> lista = dao.findAll();
        
        for(Estado e : lista){
            System.out.println(e.getNome());
            
        }
        
    }
    
}
